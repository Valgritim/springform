<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connection form</title>
</head>
<body>
<h1>Connection form</h1>
	<form:form modelAttribute="perso" action="connect" method="post">
		<form:label path="nom">nom</form:label>
		<form:input path="nom"/>
		<form:label path="prenom">pr�nom</form:label>
		<form:input path="prenom"/>
		<input type="submit" value="Connexion">
</form:form>
</body>
</html>