package org.eclipse.springform.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Entity
public class Personne {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long num;
	
	@Size(min = 2,max = 50)
	@NotEmpty(message="Le champ nom est obligatoire")
	private String nom;
	
	@Size(min = 2, max = 50)
	@NotEmpty(message="Le champ pr�nom est obligatoire")
	private String prenom;
	
	public Personne(String nom, String prenom) {
		super();		
		this.nom = nom;
		this.prenom = prenom;
	}

	public Personne() {
		super();
	}

	public Long getNum() {
		return num;
	}

	public void setNum(Long num) {
		this.num = num;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	

}
