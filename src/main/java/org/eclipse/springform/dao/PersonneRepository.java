package org.eclipse.springform.dao;

import java.util.List;

import org.eclipse.springform.model.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneRepository extends JpaRepository<Personne, Long>{
	List<Personne> findByNomAndPrenom(String nom,String prenom);
}
